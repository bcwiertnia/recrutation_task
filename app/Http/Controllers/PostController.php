<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Support\Facades\Redis;

class PostController extends Controller
{    
    public function post() {
        $data = request()->post();
        
        $object = new Post();
        $object->description = $data['content'];
        $object->save();

        Redis::set("post".$object->id, $data['content']);
    }
    
    public function getAllPosts() {
        $all = [];
        
        $redisAll = Redis::keys("*post*");
        foreach($redisAll as $k) {
            $k = str_replace("laravel_database_", "", $k);
            $key = str_replace("post", "", $k);
            $all[$key] = Redis::get($k);
        }
        ksort($all);
        $all = array_values($all);
        
        if(count($all)) return $all;
        
        foreach (Post::all() as $p) {
            $all[] = $p->description;
        }
        return $all;
    }
}
