<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                float: none;
                clear: both;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .col-md-4 {
                width: 33%;
                float: left;
            }
            
            .col-md-8 {
                width: 66%;
                float: left;
            }
            
            .col-md-4 input, .col-md-4 textarea {
                width: 95%;
            }
            
            input[type=button] {
                text-align: center;
                font-weight: bold;
                font-size: 14px;
            }
            
            #posts {
                clear: both;
            }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <script>
            jQuery(function($) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                function getAllPosts() {
                    $.post("/getAllPosts", {}, function(data) {
                        console.log(data);
                        data = JSON.parse(data);

                        $("#posts").html(data.join("<br/><br/>"));
                    }, "text");
                }
                
                $(document).ready(function() {
                    $("#counter").click(function() {
                        var l1 = $("#number_1").val()*1;
                        var l2 = $("#number_2").val()*1;
                        var r = l1+l2;
                        
                        $("#counter_result").text(r);
                    });
                    
                    $("#add_post").click(function() {
                        var content = $("#post_content").val();
                        
                        $.post("/post", {content: content}, function(data) {
//                            console.log(data);
                            getAllPosts();
                        }, "text");
                        
                        $("#post_content").val("");
                        return false;
                    });
                });
                
                getAllPosts();
            });
        </script>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Part I
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <input type="number" id="number_1"/>
                    </div>
                    <div class="col-md-4">
                        <input type="number" id="number_2"/>
                    </div>
                    <div class="col-md-4">
                        <input type="button" id="counter" value="Licz"/>
                    </div>
                </div>
                <div class="row">
                    <div id="counter_result">
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="title m-b-md">
                    Part II
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <textarea id="post_content"></textarea>
                    </div>
                    <div class="col-md-4">
                        <input type="button" id="add_post" value="Dodaj tekst"/>
                    </div>
                </div>
                <div class="row">
                    <div id="posts">
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
